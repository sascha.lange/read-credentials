package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws/session"
)

func main() {
	sess := session.Must(session.NewSession())
	// Retrieve the credentials value
	credValue, _ := sess.Config.Credentials.Get()
	fmt.Println(credValue.AccessKeyID)
	fmt.Println(credValue.SecretAccessKey)
}
